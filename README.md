# Crayons

A java library for prettifying output like the python crayons library

# What am I doing?

This is basically String concatenations along with ANSI escape characters, purpose of this library is for coloring CLI outputs for improved readability

# How to use

- Download the *jar* file [here](https://bitbucket.org/ReciForous/java-crayons/downloads/).

- import into your project `import org.crayons.Crayons`.

- **Note:** This library currently does not work with windows `cmd` as it doesn't allow ANSI escapes, if you wan to see this on windows use `powershell` or `cygwin`. 

### Some use case examples

- A normal hello world print
```
System.out.println(Crayons.green("Hello") + "world");
```

- Let there be **bold**
```
System.out.println(Crayons.green("Hello", true) + "world");
```

- Got an error?
```
try{
    ....
}
catch(Exception e){
    System.out.println(Crayons.red("Error: ") + e.getMessage())    
}
```

- Something brighter?
```
System.out.println(Crayons.brightRed("Error: ", true) + "Something died");
```

### Available colors (Normal and high intensity)

- Green
- Red
- Cyan
- Purple
- Black
- Green
- Yellow
- Blue
- White

### Styling options

- Bold

# Wanna Help?

Send me an E-mail at [mohamedzainriyaz@gmail.com](mailto:mohamedzainriyaz@gmail.com)
